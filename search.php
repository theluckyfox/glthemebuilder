<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package Theme
 */
?>
<?php get_header(); ?>                   
    <main id="main" >
        <?php 
		if (have_posts()) :  
		?>
		<h2>Результаты поиска <?= get_search_query(); ?>:</h2>
		<?php 
            while (have_posts()) : the_post();
        ?>     
        <?php 
            the_title();
            the_content();
        ?>
        <?php 
            endwhile;
        endif;
        ?>
    </main>
<?php get_footer(); ?>
