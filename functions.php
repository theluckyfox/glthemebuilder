<?php
/**
 * Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Theme
 */

/** * 
 * include all files from core folder 
 */

foreach (glob(dirname(__FILE__) . '/core/*.php') as $filename)
{
    require_once($filename); 
} 

?>

