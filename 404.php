<?php
/**
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Theme
 */

get_header();
?>
	<main id="main" class="site-main">
		<section class="error-404 not-found">
			<p>404</p>
		</section>
	</main>

<?php
get_footer();
