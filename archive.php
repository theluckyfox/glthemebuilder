<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Theme
 */
?>
<?php get_header(); ?>                   
    <main id="main" >
        <?php 
		if (have_posts()) :  
			the_archive_title( '<h2>', '</h2>' );
            while (have_posts()) : the_post();
        ?>     
        <?php 
            the_title();
            the_content();
        ?>
        <?php 
            endwhile;
        endif;
        ?>
    </main>
<?php get_footer(); ?>