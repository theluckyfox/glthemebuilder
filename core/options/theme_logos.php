<?php 

function theme_logos_register( $wp_customize ) {    
    /**
     * site logo
    */

    $wp_customize->add_section('branding' , array(
        'title'      => 'Branding',
        'priority'   => 26,
    ));

    /* site_logo_control */

    $wp_customize->add_setting('site_logo',
        array(
            'default' => '',
        )
    );

    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'site_logo_control', 
        array(
            'label'        => 'Site logo',
            'section'    => 'branding',
            'settings'   => 'site_logo',
    )));   
}

add_action('customize_register', 'theme_logos_register');

?>