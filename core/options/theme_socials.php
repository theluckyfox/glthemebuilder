<?php 

function theme_socials_register( $wp_customize ) {    
    /**
     * socials
    */

    $wp_customize->add_section('socials' , array(
        'title'      => 'Social links',
        'priority'   => 25,
    ));    

    /* socials_controls */

    /* facebook */

    $wp_customize->add_setting('facebook_setting',
        array(
            'default' => '',
        )
    );

    $wp_customize->add_control(
        'facebook_control', 
        array(
            'label'    => 'Facebook',
            'section'  => 'socials',
            'settings' => 'facebook_setting',
            'type'     => 'text'
        )
    ); 
    
    /* twitter */

    $wp_customize->add_setting('twitter_setting',
        array(
            'default' => '',
        )
    );

    $wp_customize->add_control(
        'twitter_control', 
        array(
            'label'    => 'Twitter',
            'section'  => 'socials',
            'settings' => 'twitter_setting',
            'type'     => 'text'
        )
    );  
    
    /* instagram */

    $wp_customize->add_setting('instagram_setting',
        array(
            'default' => '',
        )
    );

    $wp_customize->add_control(
        'instagram_control', 
        array(
            'label'    => 'Instagram',
            'section'  => 'socials',
            'settings' => 'instagram_setting',
            'type'     => 'text'
        )
    );  

    /* linkedin */

    $wp_customize->add_setting('linkedin_setting',
        array(
            'default' => '',
        )
    );

    $wp_customize->add_control(
        'linkedin_control', 
        array(
            'label'    => 'LinkedIn',
            'section'  => 'socials',
            'settings' => 'linkedin_setting',
            'type'     => 'text'
        )
    );  
}

add_action('customize_register', 'theme_socials_register');

?>