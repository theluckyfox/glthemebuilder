<?php

function theme_contacts_register( $wp_customize ) {    
    /**
     * socials
    */

    $wp_customize->add_section('contacts' , array(
        'title'      => 'Contacts',
        'priority'   => 27,
    ));        

    /* socials_controls */

    /* adress */

    $wp_customize->add_setting('adress_setting',
        array(
            'default' => '',
        )
    );

    $wp_customize->add_control(
        'adress_control', 
        array(
            'label'    => 'Adress',
            'section'  => 'contacts',
            'settings' => 'adress_setting',
            'type'     => 'text'
        )
    ); 
    
    /* phone */

    $wp_customize->add_setting('phone_setting',
        array(
            'default' => '',
        )
    );

    $wp_customize->add_control(
        'phone_control', 
        array(
            'label'    => 'Phone',
            'section'  => 'contacts',
            'settings' => 'phone_setting',
            'type'     => 'text'
        )
    );  
    
    /* coordinates */

    $wp_customize->add_setting('coordinates_setting',
        array(
            'default' => '',
        )
    );

    $wp_customize->add_control(
        'coordinates_control', 
        array(
            'label'    => 'Coordinates',
            'section'  => 'contacts',
            'settings' => 'coordinates_setting',
            'type'     => 'textarea'
        )
    );      
}

add_action('customize_register', 'theme_contacts_register');

?>