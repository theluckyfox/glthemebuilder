<?php

/**
 * The main file for register options
 * 
 * Enqueue scripts and styles
 */

function theme_scripts() {

	/* theme fonts */		
	
	/* theme styles */

	wp_enqueue_style( 'theme-style', get_stylesheet_uri() );

	wp_register_style('normalize', get_template_directory_uri() . '/assets/styles/normalize.css', false, '1.0'); 
	wp_enqueue_style('normalize');

	wp_register_style('media-styles', get_template_directory_uri() . '/assets/styles/media.css', false, '1.0'); 
	wp_enqueue_style('media-styles');	

	/* theme scripts */	

	wp_deregister_script('jquery');
	wp_register_script('jquery', get_template_directory_uri() . '/assets/js/jquery-3.3.1.min.js', array(), '1.0', false);
	wp_enqueue_script('jquery');	

	wp_register_script('main-scripts', get_template_directory_uri() . '/assets/js/script.js', array('jquery'), '1.0', false);
	wp_enqueue_script('main-scripts');
};

add_action( 'wp_enqueue_scripts', 'theme_scripts' );

?>