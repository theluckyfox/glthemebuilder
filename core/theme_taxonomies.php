<?php 

/**
 * The main file for register taxonomies
 * 
 * include all files from taxonomies folder 
 */

foreach (glob(dirname(__FILE__) . '/taxonomies/*.php') as $filename)
{
    require_once($filename);
}

?>