<?php 

/**
 * The main file for register post_types
 * 
 * include all files from post_types folder 
 */

foreach (glob(dirname(__FILE__) . '/post_types/*.php') as $filename)
{
    require_once($filename);
}

?>