<?php

/**
 * example for register sidebar from wp-kama
 * 
 * https://wp-kama.ru/function/register_sidebar
 */

/* add_action( 'widgets_init', 'register_sidebars' );

function register_sidebars(){
	register_sidebar( array(
		'name'          => 'Example',
		'id'            => 'example',
		'description'   => '',
		'class'         => '',
		'before_widget' => '<li id="%1$s" class="widget %2$s">',
		'after_widget'  => "</li>\n",
		'before_title'  => '<h2 class="widgettitle">',
		'after_title'   => "</h2>\n",
	) );
}

?>