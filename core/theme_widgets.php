<?php 

/**
 * The main file for register widgets
 * 
 * include all files from widgets folder 
 */

foreach (glob(dirname(__FILE__) . '/widgets/*.php') as $filename)
{
    require_once($filename);
}

?>