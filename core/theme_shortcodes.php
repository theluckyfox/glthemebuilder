<?php 

/**
 * The main file for register shortcodes
 * 
 * include all files from shortcodes folder 
 */

foreach (glob(dirname(__FILE__) . '/shortcodes/*.php') as $filename)
{
    require_once($filename);
}

?>