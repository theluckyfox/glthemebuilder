<?php 

/**
 * The main file for register options
 * 
 * include all files from options folder 
 */

foreach (glob(dirname(__FILE__) . '/options/*.php') as $filename)
{
    require_once($filename);
}

?>