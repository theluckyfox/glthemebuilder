<?php 

/**
 * The main file for register dynamic_sidebars
 * 
 * include all files from dynamic_sidebars folder 
 */

foreach (glob(dirname(__FILE__) . '/dynamic_sidebars/*.php') as $filename)
{
    require_once($filename);
}

?>