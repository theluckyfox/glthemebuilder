<?php

/**
 * example for register menu 
 * 
 * 
 * register theme header_menu
 */

add_action('after_setup_theme', 'register_header_menu');

function register_header_menu(){
	register_nav_menu('header_menu', 'Header menu');
}

/**
 * create action for outputing header menu
 */

add_action('show_header_menu', 'header_menu_func');

function header_menu_func() {
    wp_nav_menu( array(
        'theme_location'  => 'header_menu',
        'menu'            => 'Header menu', 
        'container'       => '', 
        'container_class' => '', 
        'container_id'    => '',
        'menu_class'      => '', 
        'menu_id'         => 'header-menu',
        'echo'            => true,
        'fallback_cb'     => 'wp_page_menu',
        'before'          => '',
        'after'           => '',
        'link_before'     => '',
        'link_after'      => '',
        'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
        'depth'           => 0,
        'walker'          => '',
    ) );
}

?>