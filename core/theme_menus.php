<?php 

/**
 * The main file for register menus
 * 
 * include all files from menus folder 
 */

foreach (glob(dirname(__FILE__) . '/menus/*.php') as $filename)
{
    require_once($filename);
}

?>