<?php

/**
 * The main file for default settings
 * 
 * remove emoji
 */

remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );

/**
 * add woocommerce support
 */

/* add_action( 'after_setup_theme', 'woocommerce_support' );

function woocommerce_support() {
    add_theme_support( 'woocommerce' );
} */

/**
 * add new image sizes
 * 
 * https://wp-kama.ru/function/add_image_size
 */

/* add_action( 'after_setup_theme', 'theme_images_sizes' );

function theme_images_sizes() {
    add_theme_support( 'post-thumbnails' );

	add_image_size( 'category-thumb', 300, 300, true ); 
} */

?>